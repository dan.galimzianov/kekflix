-- CreateTable
CREATE TABLE "Movie" (
    "id" SERIAL NOT NULL,
    "torrentBayId" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "imdb_code" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "title_english" TEXT NOT NULL,
    "title_long" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "year" INTEGER NOT NULL,
    "rating" INTEGER NOT NULL,
    "runtime" INTEGER NOT NULL,
    "genres" TEXT[],
    "summary" TEXT NOT NULL,
    "description_full" TEXT NOT NULL,
    "language" TEXT NOT NULL,
    "background_image" TEXT NOT NULL,
    "background_image_original" TEXT NOT NULL,
    "state" TEXT NOT NULL,

    CONSTRAINT "Movie_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Movie_torrentBayId_key" ON "Movie"("torrentBayId");
