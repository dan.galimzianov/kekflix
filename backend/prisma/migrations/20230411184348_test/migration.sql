-- CreateTable
CREATE TABLE "Test" (
    "id" SERIAL NOT NULL,
    "torrentBayId" TEXT NOT NULL,

    CONSTRAINT "Test_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Test_torrentBayId_key" ON "Test"("torrentBayId");
