/*
  Warnings:

  - Changed the type of `torrentBayId` on the `Movie` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "Movie" DROP COLUMN "torrentBayId",
ADD COLUMN     "torrentBayId" INTEGER NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Movie_torrentBayId_key" ON "Movie"("torrentBayId");
