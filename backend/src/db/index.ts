import {PrismaClient} from '@prisma/client';
import {NextFunction, Request, Response} from 'express';

const prisma = new PrismaClient({
  log: ['query'],
});

const extendContextWithDb = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  req.context = req.context || {};
  req.context.db = prisma;

  next();
};
export {prisma, extendContextWithDb};
