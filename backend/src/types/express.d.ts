import {prisma} from '@/db';

// to make the file a module and avoid the TypeScript error
export {};

declare global {
  namespace Express {
    interface User {id: number}
    export interface Request {
      context: {
        db: typeof prisma;
      };
    }
  }
}
