export namespace TorrentBayApi {

  interface Movie {
    id: number,
    url: string,
    imdb_code: string,
    title: string,
    title_english: string,
    title_long: string,
    slug: string,
    year: number,
    rating: number,
    runtime: number,
    genres: string[],
    summary: string;
    description_full: string;
    language: string,
    background_image: string,
    background_image_original: string,
    state: string,
  }

  export interface Response {
    "status": string,
    "status_message": string,
    data: {
      movie_count: number,
      limit: number,
      page_number: number,
      movies?: Movie[]
    }
  }
}
