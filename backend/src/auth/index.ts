import express from 'express';
import {validate} from '@/validation/validation.middleware';
import {authSchema, login, logout, registration} from '@/auth/auth.controller';

const authRoutes = express.Router();
authRoutes.post('/login', validate(authSchema), login);
authRoutes.post('/registration', validate(authSchema), registration);
authRoutes.post('/logout', validate(authSchema), logout);

export {authRoutes};
