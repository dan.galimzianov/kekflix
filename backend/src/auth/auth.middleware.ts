import {Request, Response} from "express";

export const authMiddleware = (req: Request, res: Response, next) => {
  if (req.session.user) {
    next()
    return;
  }
  return res.status(402).json({message: 'unauth'})
}
