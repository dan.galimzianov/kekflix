import {NextFunction, Request, Response} from 'express';
import {z} from 'zod';
import bcrypt from "bcrypt";

export const authSchema = z.object({
  body: z.object({
    email: z.string().email(),
    password: z.string(),
  }),
});

export const login = async (req: Request, res: Response, next: NextFunction) => {
  const {email, password} = req.body;
  const user = await req.context.db.user.findFirst({where: {email}});

  const isCredentialRight =
    user && (await bcrypt.compare(password, user.password));

  if (!isCredentialRight) {
    return res.status(400).json({message: 'Wrong email or password'});
  }
  req.session.user = {email: user.email, id: user.id};
  return res.status(200).json({message: 'ok'});
};
export const registration = async (
  req: Request<{}, typeof authSchema>,
  res: Response,
) => {
  const {email, password} = req.body;
  const user = req.context.db.user;
  const candidate = await user.findFirst({where: {email}});

  if (candidate) {
    return res.status(400).json({message: 'Email exists'})
  }

  const newUser = await user.create({data: {
    email,
    password: await bcrypt.hash(password, 10)
  }});
  req.session.user = {email: newUser.email, id: newUser.id};
  return res.status(200).json({message: 'ok'});
};

export const logout = async (req: Request, res: Response, next: NextFunction) => {
  req.session.user = null;
  return res.status(200).json({message: 'ok'});
};
