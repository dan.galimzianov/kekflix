import dotenv from 'dotenv';
import {z} from 'zod';
import {join} from 'node:path';
import dotenvExpand from 'dotenv-expand';

const env = dotenv.config({path: join(__dirname, '../.env')});
dotenvExpand.expand(env);

const envValidationSchema = z
  .object({
    KEKFLIX_APP_PORT: z.string(),
    PASSPORT_SECRET_KEY: z.string(),
    DATABASE_URL: z.string(),
    SESSION_KEY: z.string(),
  })
  .required();

const envInit = () => {
  envValidationSchema.parse(process.env);
};

export {envInit};
