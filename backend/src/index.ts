import express from 'express';
import {envInit} from '@/env-init';
import {routes} from './routes';
import {extendContextWithDb} from '@/db';
import morgan from 'morgan';
import session from 'express-session';

envInit();

const app = express();
app.use(morgan('dev'));

app.use(session({secret: 'keyboard cat', resave: false, saveUninitialized: false}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(extendContextWithDb);
app.use(routes);

app.listen(process.env.KEKFLIX_APP_PORT, () => {
  console.log(`Server listen port: ${process.env.KEKFLIX_APP_PORT}`);
});
