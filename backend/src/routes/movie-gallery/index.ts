import express from 'express';
import {moviesList} from "@/routes/movie-gallery/movie-gallery.controller";

const movieGalleryRoutes = express.Router();

movieGalleryRoutes.get('/list', moviesList);

export {movieGalleryRoutes};
