import {Request, Response} from "express";

const express = require('express');
const { PrismaClient } = require('@prisma/client');
const app = express();
const prisma = new PrismaClient();
export const moviesList = async (req: Request, res: Response) => {
  const { page = 1, pageSize = 10 } = req.query;

  const [ total, items ] = await Promise.all([
    prisma.movie.count(),
    prisma.movie.findMany({
      skip: (Number(page) - 1) * Number(pageSize),
      take: pageSize
    })
  ]);

  const totalPages = Math.ceil(total / Number(pageSize));

  res.json({
    total,
    totalPages,
    items
  });
}
