import express from 'express';
import {errorHandler} from './error.handler';
import {healthController} from './health.controller';
import {authRoutes} from '@/auth';
import {authMiddleware} from "@/auth/auth.middleware";
import {movieParserRoutes} from "@/routes/movie-parser";
import {movieGalleryRoutes} from "@/routes/movie-gallery";

const routes = express.Router();

routes.use('/health', healthController);
routes.use('/movie-parser', authMiddleware, movieParserRoutes);
routes.use('/movie-gallery', movieGalleryRoutes);
routes.use('/auth', authRoutes);
routes.use(errorHandler);

export {routes};
