import {prisma} from '@/db';
import axios from "axios";
import {TorrentBayApi} from "@/types/types";

enum ParserStatus {RUNNING = 'RUNNING', PAUSED = 'PAUSED'}

const LIMIT = 10;

class Parser {
  private status: ParserStatus = ParserStatus.PAUSED;
  private fetch = async () => {
    if (this.status === ParserStatus.PAUSED) {
      return;
    }

    try {
      const count = await prisma.movie.count();
      const parsedPagesCount = Math.floor(count / LIMIT);
      const lastOldMovieIdx = count - parsedPagesCount * LIMIT;

      const movies = (await axios.get<TorrentBayApi.Response>(
        `https://yts.torrentbay.to/api/v2/list_movies.json?limit=${LIMIT}&page=${parsedPagesCount + 1}`
      ))
        .data.data?.movies

      if (!movies) {
        this.stop();
        return;
      }
      const data = movies
        .filter((_, i) => lastOldMovieIdx < i)
        .map((movie) => ({
          torrentBayId: movie.id,
          url: movie.url,
          imdb_code: movie.imdb_code,
          title: movie.title,
          title_english: movie.title_english,
          title_long: movie.title_long,
          slug: movie.slug,
          year: movie.year,
          rating: movie.rating,
          runtime: movie.runtime,
          genres: movie.genres,
          summary: movie.summary,
          description_full: movie.description_full,
          language: movie.language,
          background_image: movie.background_image,
          background_image_original: movie.background_image_original,
          state: movie.state
        }))

      await prisma.movie.createMany({data})


      setTimeout(this.fetch, 500)
    } catch (err) {
      console.error(err)
    }
  }

  start = () => {
    if (this.status === ParserStatus.RUNNING) {
      return;
    }
    this.status = ParserStatus.RUNNING;
    this.fetch();
  }

  stop() {
    this.status = ParserStatus.PAUSED;
  }

  getStatus() {
    return this.status;
  }
}

export const parser = new Parser();
