import {Request, Response} from "express";
import {parser} from "@/routes/movie-parser/movie-parser";

export const startParser = (req: Request, res: Response) => {
  parser.start();
  res.json({message: 'ok'})
}

export const pauseParser = (req: Request, res: Response) => {
  parser.stop();
  res.json({message: 'ok'})
}

export const parserStatus = (_, res: Response) => {
  res.json({status: parser.getStatus()});
}
