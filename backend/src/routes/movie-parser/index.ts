import express from 'express';
import {parserStatus, pauseParser, startParser} from "@/routes/movie-parser/movier-parser.controller";

const movieParserRoutes = express.Router();

movieParserRoutes.post('/start', startParser);
movieParserRoutes.post('/pause', pauseParser);
movieParserRoutes.get('/status', parserStatus);

export {movieParserRoutes};
