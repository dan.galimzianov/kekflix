#!/bin/sh
apk add openssh git curl rsync
ssh "$CONNECTION_STR" "mkdir -p $TARGET_COPY_PATH";
rsync -e 'ssh -i ~/.ssh/ssh-private-key' -r $SERVE_PATH "$CONNECTION_STR:$TARGET_COPY_PATH";
