#!/bin/sh
apk update
mkdir -p ~/.ssh
which ssh-agent || ( apk add --update openssh )
eval "$(ssh-agent -s)"
touch ~/.ssh/ssh-private-key && echo "$SSH_PRIVATE_KEY_DEPLOYER" >> ~/.ssh/ssh-private-key
chmod 400 ~/.ssh/ssh-private-key
ssh-add ~/.ssh/ssh-private-key
[[ -f /.dockerenv ]] && echo -e "Host *\\n\\tStrictHostKeyChecking no\\n\\n" > ~/.ssh/config
